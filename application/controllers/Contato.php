<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->model('m_contato');
    }

    public function index() {
        $variaveis['telefones'] = $this->m_contato->getTelefones();
        $variaveis['emails'] = $this->m_contato->getEmails();
        $this->load->view('estrutura/e_cabecalho');
        $this->load->view('v_contato', $variaveis);
        $this->load->view('estrutura/e_rodape');
    }

}
