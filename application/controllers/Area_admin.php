<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Area_admin extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->library('session');
//        $this->load->library('upload');
        $this->load->model('m_imagens');
        $this->load->model('m_contato');
    }

    public function index() {
        $this->load->view('estrutura/admin_cabecalho');
        $this->load->view('admin/v_home');
        $this->load->view('estrutura/admin_rodape');
    }

//        SELECT DATE(`data`) FROM `banheiros` WHERE 1
    public function banheiros() {
        $variaveis['tipo'] = "banheiros";
        $this->loadView($variaveis);
    }

    public function bares_restaurantes() {
        $variaveis['tipo'] = "baresrestaurantes";
        $this->loadView($variaveis);
    }

    public function closets() {
        $variaveis['tipo'] = "closets";
        $this->loadView($variaveis);
    }

    public function cozinhas() {
        $variaveis['tipo'] = "cozinhas";
        $this->loadView($variaveis);
    }
    
    public function diversos() {
        $variaveis['tipo'] = "diversos";
        $this->loadView($variaveis);
    }

    public function dormitorios() {
        $variaveis['tipo'] = "dormitorios";
        $this->loadView($variaveis);
    }

    public function escritorios() {
        $variaveis['tipo'] = "escritorios";
        $this->loadView($variaveis);
    }
   
    public function funcionarios() {
        $variaveis['tipo'] = "funcionarios";
        $this->loadView($variaveis);
    }

    public function lavanderias() {
        $variaveis['tipo'] = "lavanderias";
        $this->loadView($variaveis);
    }

    public function lojas() {
        $variaveis['tipo'] = "lojas";
        $this->loadView($variaveis);
    }

    public function salas() {
        $variaveis['tipo'] = "salas";
        $this->loadView($variaveis);
    }

//    SERVIÇOS

    public function corte() {
        $variaveis['tipo'] = "corte";
        $this->loadView($variaveis);
    }

    public function corte_cnc_3d() {
        $variaveis['tipo'] = "corte_cnc_3d";
        $this->loadView($variaveis);
    }

    public function laminacao() {
        $variaveis['tipo'] = "laminacao";
        $this->loadView($variaveis);
    }

    public function pintura_laqueada() {
        $variaveis['tipo'] = "pintura_laqueada";
        $this->loadView($variaveis);
    }
    
//    FAÇA VOCÊ
    public function chapas() {
        $variaveis['tipo'] = "chapas";
        $this->loadView($variaveis);
    }
    
    public function ferragens() {
        $variaveis['tipo'] = "ferragens";
        $this->loadView($variaveis);
    }
    
    public function vidros() {
        $variaveis['tipo'] = "vidros";
        $this->loadView($variaveis);
    }

    private function loadView($variaveis) {
        $variaveis["imagens"] = $this->m_imagens->getAll($variaveis['tipo']);
        $this->load->view('estrutura/admin_cabecalho');
        $this->load->view('estrutura/admin_produtos', $variaveis);
        $this->load->view('estrutura/admin_rodape');
    }

    public function telefones() {
        $variaveis['telefones'] = $this->m_contato->getTelefones();
        $this->load->view('estrutura/admin_cabecalho');
        $this->load->view('v_telefones', $variaveis);
        $this->load->view('estrutura/admin_rodape');
    }

    public function deleta_tel() {
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
            $this->m_contato->deletaTelefones($id);
        }
        $this->telefones();
    }

    public function add_tel() {
        if ($this->input->post('numero')) {
            $num['numero'] = $this->input->post('numero');
            $this->m_contato->addTelefones($num);
        }
        $this->telefones();
    }
    public function emails() {
        $variaveis['emails'] = $this->m_contato->getEmails();
        $this->load->view('estrutura/admin_cabecalho');
        $this->load->view('v_emails', $variaveis);
        $this->load->view('estrutura/admin_rodape');
    }

    public function deleta_email() {
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
            $this->m_contato->deletaEmails($id);
        }
        $this->emails();
    }

    public function add_email() {
        if ($this->input->post('email')) {
            $email['email'] = $this->input->post('email');
            $this->m_contato->addEmails($email);
        }
        $this->emails();
    }

    public function salva_tipo() {
        $tipo = $this->input->post('tipo');
//        ATRIBUI UM NOME UNICO E CHECA NA TABELA
        $dados['img'] = $this->attrNome($tipo);

        $configUpload['upload_path'] = upload_folder($tipo);            #the folder placed in the root of project
        $configUpload['file_name'] = $dados['img'];
        $configUpload['allowed_types'] = 'gif|jpg|png|bmp|jpeg';       #allowed types description
        $configUpload['max_size'] = '0';                          #max size
        $configUpload['max_width'] = '0';                          #max width
        $configUpload['max_height'] = '0';                          #max height
        $configUpload['encrypt_name'] = false;                         #encrypt name of the uploaded file

        $this->load->library('upload', $configUpload);                  #init the upload class
        if (!$this->upload->do_upload('fileToUpload')) {
            $uploadedDetails = $this->upload->display_errors();
            echo $configUpload['upload_path'];
        } else {
            $uploadedDetails = $this->upload->data();

//            ADICIONA A EXTENSÃO AO FINAL DO NOME
            $dados['img'] .= $uploadedDetails['file_ext'];
//            ADICIONA NOME COMPLETO NO BANCO
            $this->m_imagens->add($dados, $tipo);
        }
//        print_r($uploadedDetails);
//        die;
		$tipo = str_replace("baresrestaurantes", "bares_restaurantes", $tipo);
        redirect('area_admin/' . $tipo);
    }

    private function attrNome($tipo) {
        $file_name = uniqid();
        if ($this->m_imagens->testaNome($file_name, $tipo) == FALSE) {
            $file_name = $this->attrNome();
        } return $file_name;
    }

    public function deleta_tipo() {
        $nome = $this->input->post('imgNome');
        $tipo = $this->input->post('tipo');
        $id = $this->m_imagens->getId($nome, $tipo);
        if ($this->deleta_img($nome, $tipo)) {
            echo "SO DELETO";
            if ($this->m_imagens->deleta($id, $tipo)) {
                
            }
			$tipo = str_replace("baresrestaurantes", "bares_restaurantes", $tipo);
            redirect('area_admin/' . $tipo);
        }
//        else {
        //NÃO APAGOU MOSTRAR ERROS
//        }
    }

    private function deleta_img($nome, $tipo) {
        if ($nome && $tipo) {
            if (is_file(upload_folder($tipo) . '/' . $nome)) {

                return unlink(upload_folder($tipo) . '/' . $nome);
            }
        }
        return FALSE;
    }

}
