<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent:: __construct();
    }

    public function index() {
        $this->load->view('estrutura/e_cabecalho');
        $this->load->view('v_home');
        $this->load->view('estrutura/e_rodape');
    }

}