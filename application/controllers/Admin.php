<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->library('session');
        $this->load->model('m_admin');
    }

    public function autentica() {
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');

        $admin = new M_admin();
        $admin->setEmail($email);
        $admin->setSenha($senha);


        if ($this->m_admin->autentica($admin) === TRUE) {
            $dadosCompletos = $this->m_admin->getDados($email);
            $_SESSION['admin']['email'] = $dadosCompletos->email;
            $_SESSION['admin']['nome'] = $dadosCompletos->nome;
            $_SESSION['admin']['erro'] = '';

            echo '<p class="blue">Entrando...</p>';
            echo '<script>
                    function entrar() {
                        window.location.replace("' . base_url("area_admin") . '");
                    }
                    entrar();
                </script>';
        } else {
            echo '<p class="red">Dados incorretos!</p>';
        }
    }

    public function cadastra($nome = null, $email = null, $senha = null) {
        $dados['nome'] = "mauricio";
        $dados['email'] = "mail";
        $dados['senha'] = "p4ss";
        $this->m_admin->cadastra($dados);
    }
    
    public function sair(){
        $this->session->sess_destroy();
        redirect("area_admin");
    }

}
