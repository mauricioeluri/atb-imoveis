<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Servicos extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->model('m_imagens');
    }

    public function index() {
        redirect();
    }
    
    public function corte() {
        $variaveis['tipo'] = "corte";
        $this->loadViewServ($variaveis);
    }
    public function corte_cnc_3d() {
        $variaveis['tipo'] = "corte_cnc_3d";
        $this->loadViewServ($variaveis);
    }
    public function laminacao() {
        $variaveis['tipo'] = "laminacao";
        $this->loadViewServ($variaveis);
    }
    public function pintura_laqueada() {
        $variaveis['tipo'] = "pintura_laqueada";
        $this->loadViewServ($variaveis);
    }    

    private function loadViewServ($variaveis) {
        $variaveis["imagens"] = $this->m_imagens->getAll($variaveis['tipo']);
        $this->load->view('estrutura/e_cabecalho');
        $this->load->view('v_produtos', $variaveis);
        $this->load->view('estrutura/e_rodape');
    }
}
