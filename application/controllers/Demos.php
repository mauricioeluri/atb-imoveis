<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Demos extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        //$this->load->library(array('session'));
        //$this->load->model('M_teste');
    }

    public function index() {
        $variaveis['heading'] = "Cabeçalho";
        $variaveis['message'] = "Mensagem";
        $this->load->view('errors/html/error_404', $variaveis);
    }

}
