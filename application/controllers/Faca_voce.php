<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faca_voce extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->model('m_imagens');
    }
    
    public function index() {
        redirect();
    }
    
    public function chapas() {
        $variaveis['tipo'] = "chapas";
        $this->loadViewServ($variaveis);
    } 
    
    public function ferragens() {
        $variaveis['tipo'] = "ferragens";
        $this->loadViewServ($variaveis);
    } 
    
    public function vidros() {
        $variaveis['tipo'] = "vidros";
        $this->loadViewServ($variaveis);
    } 
    
    private function loadViewServ($variaveis) {
        $variaveis["imagens"] = $this->m_imagens->getAll($variaveis['tipo']);
        $this->load->view('estrutura/e_cabecalho');
        $this->load->view('v_produtos', $variaveis);
        $this->load->view('estrutura/e_rodape');
    }
}
