<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->model('m_imagens');
    }

    public function index() {
        redirect();
    }

    public function banheiros() {
        $variaveis['tipo'] = "banheiros";
        $this->loadViewProd($variaveis);
    }

    public function bares_restaurantes() {
        $variaveis['tipo'] = "baresrestaurantes";
        $this->loadViewProd($variaveis);
    }

    public function chapas() {
        $variaveis['tipo'] = "chapas";
        $this->loadViewProd($variaveis);
    }
    
    public function closets() {
        $variaveis['tipo'] = "closets";
        $this->loadViewProd($variaveis);
    }

    public function cozinhas() {
        $variaveis['tipo'] = "cozinhas";
        $this->loadViewProd($variaveis);
    }

    public function diversos() {
        $variaveis['tipo'] = "diversos";
        $this->loadViewProd($variaveis);
    }
    
    public function dormitorios() {
        $variaveis['tipo'] = "dormitorios";
        $this->loadViewProd($variaveis);
    }
    
    public function escritorios() {
        $variaveis['tipo'] = "escritorios";
        $this->loadViewProd($variaveis);
    }
    
    public function ferragens() {
        $variaveis['tipo'] = "ferragens";
        $this->loadViewProd($variaveis);
    }

    public function lavanderias() {
        $variaveis['tipo'] = "lavanderias";
        $this->loadViewProd($variaveis);
    }

    public function lojas() {
        $variaveis['tipo'] = "lojas";
        $this->loadViewProd($variaveis);
    }

    public function salas() {
    $variaveis['tipo'] = "salas";
        $this->loadViewProd($variaveis);
    }
    
    public function vidros() {
        $variaveis['tipo'] = "vidros";
        $this->loadViewProd($variaveis);
    }

    private function loadViewProd($variaveis) {
        $variaveis["imagens"] = $this->m_imagens->getAll($variaveis['tipo']);
        $this->load->view('estrutura/e_cabecalho');
        $this->load->view('v_produtos', $variaveis);
        $this->load->view('estrutura/e_rodape');
    }

}
