<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function asset_url($param = null) {
    return base_url() . 'assets/' . $param;
}

function asset_js($param = null) {
    return '<script type="text/javascript" src="' . asset_url('js/' . $param) . '.js"></script>';
}

function asset_css($param = null) {
    return link_tag('assets/css/' . $param . '.css');
}

function asset_img($param = null) {
    return base_url('assets/img/' . $param);
}

function upload_folder($param = null) {
    return './uploads/' . $param;
}

function banheiro_img($param = null) {
    if ($param) {
        return base_url('uploads/banheiros/'.$param);
    }
    return upload_folder('banheiros/' . $param);
}

function bar_img($param = null) {
    return upload_folder('bares/' . $param);
}

function closet_img($param = null) {
    return upload_folder('closets/' . $param);
}

function cozinha_img($param = null) {
    return upload_folder('cozinhas/' . $param . '/');
}

function dormitorio_img($param = null) {
    return upload_folder('dormitorios/' . $param . '/');
}

function escritorio_img($param = null) {
    return upload_folder('escritorios/' . $param . '/');
}

function lavanderia_img($param = null) {
    return upload_folder('lavanderias/' . $param . '/');
}

function restaurante_img($param = null) {
    return upload_folder('restaurantes/' . $param);
}

function sala_img($param = null) {
    return upload_folder('salas/' . $param);
}
