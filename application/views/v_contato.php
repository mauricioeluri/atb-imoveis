<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<div class="row">
    <div class="box">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">
                <strong>Contante-nos</strong>
            </h2>
            <hr>
            <div class="col-lg-6 col-lg-offset-4">
                <div class="col-lg-4"><p style="font-size: 20px;"><i class="fa fa-phone fa-5x" aria-hidden="true"></i></p></div>
                <div class="col-lg-8">
                    <p>
                        <?php
                        foreach ($telefones->result() as $tel) {
                            echo$tel->numero."<br/>";
                        }
                        ?>
                    </p>
                </div>
            </div>
            <br />
            <div class="col-lg-6 col-lg-offset-4">
                <div class="col-lg-4"><p style="font-size: 20px;"><i class="fa fa-envelope fa-5x" aria-hidden="true"></i></p></div>
                <div class="col-lg-8">
                    <p>
                        <?php
                        foreach ($emails->result() as $em) {
                            echo$em->email."<br/>";
                        }
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>            
<div class="row">
    <div class="box">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">
                <strong>Localização</strong>
            </h2>
            <hr>
            <hr class="visible-xs">
            <!--<p>-->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1700.598179784076!2d-54.10802973158942!3d-31.331331613971816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8485b351a33565f6!2sPrefeitura+Municipal+de+Bag%C3%A9!5e0!3m2!1sen!2sbr!4v1477736748125" width="100%" height="450px" frameborder="1" style="border:1px solid black" allowfullscreen></iframe>
            <!--</p>-->

        </div>
    </div>
</div>