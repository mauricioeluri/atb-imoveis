<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
    <div class="box">
        <div class="col-lg-12 text-center">
            <div id="carousel-example-generic" class="carousel slide">
                <!-- Indicators -->
                <ol class="carousel-indicators hidden-xs">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    <!--<li data-target="#carousel-example-generic" data-slide-to="4"></li>-->
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img class="img-responsive img-full" src="<?= asset_img("slides/slide-1C.jpeg") ?>" alt="">
                    </div>
                    <div class="item">
                        <img class="img-responsive img-full" src="<?= asset_img("slides/slide-2C.jpeg") ?>" alt="">
                    </div>
                    <div class="item">
                        <img class="img-responsive img-full" src="<?= asset_img("slides/slide-3C.jpeg") ?>" alt="">
                    </div>
                    <div class="item">
                        <img class="img-responsive img-full" src="<?= asset_img("slides/slide-4C.jpeg") ?>" alt="">
                    </div>
                    <!--                                <div class="item">
                                                        <img class="img-responsive img-full" src="<?= asset_img("slides/slide-5.JPG") ?>" alt="">
                                                    </div>-->
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="icon-prev"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="icon-next"></span>
                </a>
            </div>
            <!--                        <h2 class="brand-before">
                                        <small>Welcome to</small>
                                    </h2>
                                    <h1 class="brand-name">Business Casual</h1>
                                    <hr class="tagline-divider">
                                    <h2>
                                        <small>By
                                            <strong>Start Bootstrap</strong>
                                        </small>
                                    </h2>-->
        </div>
    </div>
</div>

<div class="row">
    <div class="box">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">Construímos seu móvel
                <strong> do jeito que você deseja</strong>
            </h2>
            <hr>
            <img class="img-responsive img-border img-left" src="<?= asset_img("intro-pic.jpg") ?>" alt="">
            <hr class="visible-xs">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>

        </div>
    </div>
</div>            