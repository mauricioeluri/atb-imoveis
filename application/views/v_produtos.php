<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="row">
        <div class="col-lg-12">
            <?php
            if ($imagens->result()) {
                foreach ($imagens->result() as $imagem) {
                    ?>
                    <div class = "row">
                        <div class = "col-md-6 col-md-offset-3" style = "margin-top:20px;">
                            <?= '<img src="' . base_url('uploads') . '/' . $tipo . '/' . $imagem->img . '" style = "width:100%; height: 400px; border: 2px solid black;"/>' ?>
                        </div>
                    </div><br />
                    <?php
                }
            } else {
                ?>
                    <div class="box"><div class="row text-center" style="margin:30px"><p>Não existem imagens nesta categoria</p></div></div>
                <?php
            }
            ?>
        
    </div>
</div>