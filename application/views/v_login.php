<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Login Área Administrativa</title>
        <?= asset_css("login") ?>
        <?= asset_js("jquery-2.1.4.min") ?>
        <?= asset_js("login.min") ?>
        <?= asset_css("bootstrap.min") ?>
        <style>
            body {
                background-color: #eee;
                background-image: url("<?= asset_img("bg-admin.jpeg") ?>");
                background-repeat: no-repeat;
                background-size: 100% 120%;
            }
        </style>
    </head>
    <body>
        <div id="form_container" class='container-fluid'>
            <!--<form action="../includes/controller/UsuarioController.php" method="post" name="frm_login">-->
            <?= form_open(base_url("admin/autentica"), 'name="frm_login"') ?>
            <div class="container">
                <input type="hidden" name="acao" value="autenticar" />
                <h3>Efetuar login:</h3>
                <input type="text" name="email" id="email" placeholder="E-mail" autofocus required class='form-control'/>
                <input type="password" name="senha" id="senha" placeholder="Senha" required class="form-control"/>
                <div id="resultado"></div>
                <button type="submit" name="btn_login" class="btn btn-primary btn-lg btn-block">Entrar</button>
            </div>
            </form>
        </div>
    </body>
</html>
<script>
    $(document).ready(function () {
        $("button[name=btn_login]").click(function (e) {
            $email = $("#email");
            $senha = $("#senha");
            if ($email.val() == "") {
                $email.focus();
                return; //retorna nulo
            } else if ($senha.val() == "") {
                $senha.focus();
                return;
            }
            //PASSOU! GO AJAX!
            else {
                $("#resultado").html("Autenticando...");
                e.preventDefault();
                /**Função ajax nativa da jQuery, onde passamos como par�metro o endere�o do arquivo que queremos chamar, os dados que ir� receber, e criamos de forma encadeada a fun��o que ir� armazenar o que foi retornado pelo servidor, para poder se trabalhar com o mesmo */
                $.post("<?= base_url('admin/autentica') ?>", {email: $email.val(), senha: $senha.val()},
                        function (retorno) {
                            $("#resultado").html(retorno);
                        } //function(retorno)
                ); //$.post()
            }
        });
    });
</script>