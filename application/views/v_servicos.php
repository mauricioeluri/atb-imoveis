<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
            <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                        <hr>
                        <h2 class="intro-text text-center">Construímos seu móvel
                            <strong> do jeito que você deseja</strong>
                        </h2>
                        <hr>
                        <img class="img-responsive img-border img-left" src="<?= asset_img("intro-pic.jpg") ?>" alt="">
                        <hr class="visible-xs">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>

                    </div>
                </div>
            </div>