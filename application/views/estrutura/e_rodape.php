<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</div>
<!-- /.container -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-center col-lg-offset-3">
                <p>Copyright &copy; ATB 2016     
                    <button id="AreaAdmin" class="btn btn-link" style="margin-left: 30px;"><i class="fa fa-lock" aria-hidden="true"></i> Area admin</button>
                    <button class="btn btn-primary"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></button>
                </p>
            </div>
        </div>
    </div>
</footer>

<?= asset_js("jquery-2.1.4.min") ?>
<?= asset_js("bootstrap.min") ?>
<?= asset_css("font-awesome.min") ?>



<!-- Script to Activate the Carousel -->
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    });
    $('#AreaAdmin').click(function () {
        window.open("<?= base_url("area_admin")?>", '_blank');
    });
</script>

</body>

</html>