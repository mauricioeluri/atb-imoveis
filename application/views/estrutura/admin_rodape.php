<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?></div>
<div class="clear"></div>

<style>
    html {
        height: 100%;
        box-sizing: border-box;
    }
    body{
        position: relative;
        min-height: 100%;
    }
    #admFoo {
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        padding: 1rem;
        height:60px;
    }
    .clear{     
        height:60px;
    } h1{
        text-shadow:none;
        color:white;
    } p {
        font-family: verdana;
    }
</style>
<footer id="admFoo"><p>Copyright © ATB 2016</p></footer>
    <?= asset_css("main_admin") ?>
    <?= asset_js("bootstrap.min") ?>
    <?= asset_js("main_admin.min") ?>
    <?= asset_css("fonts") ?>
    <?= asset_css("font-awesome.min") ?>
</body>
</html>

