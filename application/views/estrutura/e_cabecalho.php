<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Maurício M. El Uri">

        <title>ATB Móveis Planejados</title>

        <?= asset_css("bootstrap.min") ?>

        <?= asset_css("business-casual") ?>
        <?= asset_css("style") ?>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <style>
        @media (max-width: 995px){
           .navbar-default .navbar-nav > li > a{
                padding: 23px;
            }
        }
        @media only screen and (max-width: 426px){
            .navbar-brand {
                font-size: 14px !important;
            }
        }
        body{
            background: url('<?= asset_img("bg-site.jpg") ?>');
        }
    </style>
    <body>

        <div class="brand"><a href="<?= base_url("home") ?>"><img id="logo" src="<?= asset_img("logoAtbC.png") ?>"/></a></div>
        <!--Nilton Vaz Cachapuz 115-->
        <div class="address-bar">Nilton Vaz Cachapuz 115 | Bagé, RS Brasil | (53)3241-6567</div>

        <!-- Navigation -->
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                    <a class="navbar-brand" href="<?= base_url("home") ?>">ATB Móveis Planejados</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produtos <b class="caret"></b></a>
                            <ul class="dropdown-menu">                                
                                <li><a href="<?= base_url("produtos/banheiros") ?>" class="link">Banheiros</a></li>
                                <li><a href="<?= base_url("produtos/bares_restaurantes") ?>" class="link">Bares e<br />Restaurantes</a></li>
                                <li><a href="<?= base_url("produtos/closets") ?>" class="link">Closets</a></li>
                                <li><a href="<?= base_url("produtos/cozinhas") ?>" class="link">Cozinhas</a></li>
                                <li><a href="<?= base_url("produtos/diversos") ?>" class="link">Diversos</a></li>
                                <li><a href="<?= base_url("produtos/dormitorios") ?>" class="link">Dormitórios</a></li>
                                <li><a href="<?= base_url("produtos/escritorios") ?>" class="link">Escritórios</a></li>
                                <li><a href="<?= base_url("produtos/lavanderias") ?>" class="link">Lavanderias</a></li>
                                <li><a href="<?= base_url("produtos/lojas") ?>" class="link">Lojas</a></li>
                                <li><a href="<?= base_url("produtos/salas") ?>" class="link">Salas</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Serviços <b class="caret"></b></a>
                            <ul class="dropdown-menu">                                
                                <li><a href="<?= base_url("servicos/corte") ?>" class="link">Corte</a></li>
                                <li><a href="<?= base_url("servicos/corte_cnc_3d") ?>" class="link">Corte CNC 3D</a></li>
                                <li><a href="<?= base_url("servicos/laminacao") ?>" class="link">Laminação</a></li>
                                <li><a href="<?= base_url("servicos/pintura_laqueada") ?>" class="link">Pintura<br/>Laqueada</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Faça você mesmo <b class="caret"></b></a>
                            <ul class="dropdown-menu">                                
                                <li><a href="<?= base_url("faca_voce/chapas") ?>" class="link">Chapas</a></li>
                                <li><a href="<?= base_url("faca_voce/ferragens") ?>" class="link">Ferragens</a></li>
                                <li><a href="<?= base_url("faca_voce/vidros") ?>" class="link">Vidros</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?= base_url("contato") ?>">Contato</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <div class="container">