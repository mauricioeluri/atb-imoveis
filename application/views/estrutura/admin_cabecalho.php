<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php
if (!isset($_SESSION['admin'])) {
    redirect("login");
}
?>
<style>
    .navbar-nav{
        text-shadow:none;
    }

    

    @media (max-width: 1234px) {
        .dropdown-toggle.link{
            font-size: 15px;
        }
    }
    @media (min-width: 768px) and (max-width: 1100px) {
        .navbar-brand {
            display: none;
        }
    }

    @media (min-width: 767px) and (max-width: 783px) {
        .navbar-default .navbar-nav > li > a{
            padding: 13px;
        }
    }
</style>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <?= asset_css("bootstrap.min") ?>
        <?= asset_css("estrutura_admin") ?>
        <?= asset_js("jquery-2.1.4.min") ?>
        <!--[if lt IE 9]>
            <script>
        alert('Você está usando um navegador antigo!\n\nAlguns itens podem não aparecer corretamente.\nConsidere atualizar para uma versão mais recente.');
        </script>
        <![endif]-->
    </head>
    <body style="background-image: url('<?= asset_img("fundoAdmin.png") ?>');">
        <nav class="navbar navbar-default navbar-static-top">
            <div id='topoTudo'>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a id="log" href="<?= base_url("area_admin") ?>">

                        <p class="navbar-brand">Area administrativa</p></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <div id='topoItem'>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle link" data-toggle="dropdown">Produtos <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= base_url("area_admin/banheiros") ?>" class="link">Banheiros</a></li>
                                    <li><a href="<?= base_url("area_admin/bares_restaurantes") ?>" class="link">Bares e<br />Restaurantes</a></li>
                                    <li><a href="<?= base_url("area_admin/closets") ?>" class="link">Closets</a></li>
                                    <li><a href="<?= base_url("area_admin/cozinhas") ?>" class="link">Cozinhas</a></li>
                                    <li><a href="<?= base_url("area_admin/diversos") ?>" class="link">Diversos</a></li>
                                    <li><a href="<?= base_url("area_admin/dormitorios") ?>" class="link">Dormitórios</a></li>
                                    <li><a href="<?= base_url("area_admin/escritorios") ?>" class="link">Escritórios</a></li>
                                    <li><a href="<?= base_url("area_admin/lavanderias") ?>" class="link">Lavanderias</a></li>
                                    <li><a href="<?= base_url("area_admin/lojas") ?>" class="link">Lojas</a></li>
                                    <li><a href="<?= base_url("area_admin/salas") ?>" class="link">Salas</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle link" data-toggle="dropdown">Serviços <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= base_url("area_admin/corte") ?>" class="link">Corte</a></li>
                                    <li><a href="<?= base_url("area_admin/corte_cnc_3d") ?>" class="link">Corte CNC 3D</a></li>
                                    <li><a href="<?= base_url("area_admin/laminacao") ?>" class="link">Laminação</a></li>
                                    <li><a href="<?= base_url("area_admin/pintura_laqueada") ?>" class="link">Pintura<br/>Laqueada</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle link" data-toggle="dropdown">Faça você mesmo <b class="caret"></b></a>
                                <ul class="dropdown-menu">                                
                                    <li><a href="<?= base_url("area_admin/chapas") ?>" class="link">Chapas</a></li>
                                    <li><a href="<?= base_url("area_admin/ferragens") ?>" class="link">Ferragens</a></li>
                                    <li><a href="<?= base_url("area_admin/vidros") ?>" class="link">Vidros</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle link" data-toggle="dropdown">Contato <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= base_url("area_admin/emails") ?>" class="link">E-mails</a></li>
                                    <li><a href="<?= base_url("area_admin/telefones") ?>" class="link">Telefones</a></li>
                                </ul>
                            </li>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-user"></i>
                                        <strong> <?= $_SESSION['admin']['nome'] ?></strong> <i class="fa fa-sort-desc"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="navbar-login">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <p class="text-center">
                                                            <i class="fa fa-user-secret fa-4x"></i>
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <p>
                                                            <a href="#" class="btn btn-primary btn-block btn-sm semborda disabled">Atualizar dados</a>
                                                            <a href="<?= base_url('admin/sair') ?>" class="btn btn-danger btn-block semborda">Sair</a>
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </ul>

                    </div>
                </div>
            </div>
        </nav>
        <div id='content'>
