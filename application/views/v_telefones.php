<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .filterable {
        margin-top: 45px;
    }
    .filterable .panel-heading .pull-right {
        margin-top: -20px;
    }
    .filterable .filters input[disabled] {
        background-color: transparent;
        border: none;
        cursor: auto;
        box-shadow: none;
        padding: 0;
        height: auto;
    }
    .filterable .filters input[disabled]::-webkit-input-placeholder {
        color: #333;
    }
    .filterable .filters input[disabled]::-moz-placeholder {
        color: #333;
    }
    .filterable .filters input[disabled]:-ms-input-placeholder {
        color: #333;
    }
    table > *, th {
        text-align: center;
    }

</style>
<div class="container">
    <div class="row col-md-6 col-md-offset-3">
        <div class="panel panel-primary filterable">

            <div class="panel-heading">
                <h3 class="panel-title">TELEFONES</h3>                
            </div>
            <table class="table">
                <thead>
                    <tr class="filters">
                        <th>Número</th>
                        <th>Opções</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($telefones->result() as $tel) {
                        echo form_open(base_url("area_admin/deleta_tel"));
                        ?>
                        <tr>
                            <td><?= $tel->numero ?></td>
                    <input type="hidden" name="id" value="<?= $tel->id ?>"/>
                    <td><button class="btn btn-danger" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                    </tr>
                    <?php
                        echo form_close();
                }
                ?>
                <tr>
                    <?= form_open(base_url("area_admin/add_tel")) ?>
                    <td><input type="tel" class="form-control" placeholder="Adicionar número" name="numero"/></td>
                    <td><button class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i></button></td>
                    <?= form_close() ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>