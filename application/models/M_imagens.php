<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_imagens extends CI_Model {

    function getAll($tipo) {
        $this->db->from($tipo);
        return $this->db->get();
    }

    function testaNome($img, $tipo) {
        $this->db->where('img', $img);
        $query = $this->db->get($tipo);
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function add($dados, $tipo) {
        if ($dados) {
            if ($this->db->insert($tipo, $dados)) {
                return true;
            } return false;
        } return false;
    }

    function deleta($id, $tipo) {
        if ($id) {
            return $this->db->where('id', $id)->delete($tipo);
        } return FALSE;
    }

    function getId($img, $tipo) {
        if ($img) {
            $this->db->from($tipo);
            return $this->db->where('img', $img)->get()->row('id');
        }
    }

}
