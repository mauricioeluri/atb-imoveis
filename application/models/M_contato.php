<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_contato extends CI_Model {

    public function getTelefones() {
        $this->db->from('telefones');
        return $this->db->get();
    }
    
    function deletaTelefones($id) {
        if ($id) {
            return $this->db->where('id', $id)->delete('telefones');
        } return FALSE;
    }
    
    function addTelefones($num) {
        if ($num) {
            if ($this->db->insert('telefones', $num)) {
                return true;
            } return false;
        } return false;
    }
    
    public function getEmails() {
        $this->db->from('emails');
        return $this->db->get();
    }
    
    function deletaEmails($id) {
        if ($id) {
            return $this->db->where('id', $id)->delete('emails');
        } return FALSE;
    }
    
    function addEmails($email) {
        if ($email) {
            if ($this->db->insert('emails', $email)) {
                return true;
            } return false;
        } return false;
    }

}
