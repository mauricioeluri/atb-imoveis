<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

    var $id;
    var $nome;
    var $email;
    var $senha;

//    ID
    function getId() {
        return $this->id;
    }

    function setId($valor) {
        $this->id = $valor;
    }

//    NOME
    function getNome() {
        return $this->nome;
    }

    function setNome($valor) {
        $this->nome = $valor;
    }

//    E-MAIL
    function getEmail() {
        return $this->email;
    }

    function setEmail($valor) {
        $this->email = $valor;
    }

//    SENHA
    function getSenha() {
        return $this->senha;
    }

    function setSenha($valor) {
        $this->senha = $valor;
    }

    function autentica($admin) {
        $email = $admin->getEmail();
        $senha = $admin->getSenha();
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->db->where('email', $email);
        }

        $qr = $this->db->get('funcionarios');

        foreach ($qr->result() as $row) {
            if ($this->verify_password_hash($senha, $row->senha)) {
                return true;
            }
        }
        return false;
    }

    function cadastra($dados) {
        $dados['senha'] = $this->hash_password($dados['senha']);
        return $this->db->insert("funcionarios", $dados);
    }

    function getDados($email) {
        $this->db->from('funcionarios');
        $this->db->like('email', $email);
        foreach ($this->db->get()->result() as $admin) {
            return $admin;
        }
        return FALSE;
    }

    /**
     * hash_password function.
     *
     * @access private
     * @param mixed $password
     * @return string|bool could be a string on success, or bool false on failure
     */
    private function hash_password($password) {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * verify_password_hash function.
     *
     * @access private
     * @param mixed $password
     * @param mixed $hash
     * @return bool
     */
    private function verify_password_hash($password, $hash) {
        return password_verify($password, $hash);
    }

}
